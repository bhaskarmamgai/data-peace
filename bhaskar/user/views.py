
from rest_framework import filters

from user.models import *
# api views
from rest_framework import generics

from .serializers import UserSerializer


class UsersView(generics.ListCreateAPIView):
    serializer_class = UserSerializer

    ordering_fields = '__all__'

    search_fields = ('id','first_name','last_name','company_name','city','state','zip','email','web','age',)
    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        first_name = self.request.query_params.get('first_name', None)
        last_name = self.request.query_params.get('last_name', None)
        company_name = self.request.query_params.get('company_name', None)
        age = self.request.query_params.get('age', None)
        state = self.request.query_params.get('state', None)
        zip = self.request.query_params.get('zip', None)
        email = self.request.query_params.get('email', None)
        web = self.request.query_params.get('web', None)
        city = self.request.query_params.get('city', None)

        user_obj = User.objects.all()

        if id:
            user_obj =user_obj.filter(id=id)
        if first_name:
            user_obj =user_obj.filter(first_name=first_name)
        if last_name:
            user_obj =user_obj.filter(last_name=last_name)
        if company_name:
            user_obj =user_obj.filter(company_name=company_name)
        if city:
            user_obj =user_obj.filter(city=city)
        if state:
            user_obj =user_obj.filter(state=state)
        if zip:
            user_obj =user_obj.filter(zip=zip)
        if email:
            user_obj =user_obj.filter(email=email)
        if web:
            user_obj =user_obj.filter(web=web)
        if age:
            user_obj =user_obj.filter(age=age)
        return user_obj

    def perform_create(self, serializer):
        serializer.save()


class UserDetailsView(generics.RetrieveUpdateDestroyAPIView):
    user_obj = User.objects.all()
    serializer_class = UserSerializer
